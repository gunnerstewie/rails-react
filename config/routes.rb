# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'users#index'
  namespace :api do
    resources :users
  end

  resources :users
end
