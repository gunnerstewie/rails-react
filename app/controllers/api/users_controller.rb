# frozen_string_literal: true

module Api
  class UsersController < ApplicationController
    def index
      users = User.all

      render json: users
    end

    def new
      user = User.new

      render json: user
    end

    def create
      user = User.new permitted_params

      user.save
      render json: user
    end

    def edit
      user = User.find(params[:id])

      render json: user
    end

    def update
      user = User.find(params[:id])

      user.update permitted_params
      render json: user
    end

    def destroy
      user = User.find(params[:id])
      user.destroy
      head :no_content, status: :ok
    end

    private

    def permitted_params
      params.require(:user).permit(:user_name, :user_email, :birthdate, :phone_number)
    end
  end
end
