# frozen_string_literal: true

class UsersController < ApplicationController
  def index
    users = User.all

    locals users: users
  end

  def new
    user = User.new

    locals user: user
  end

  def create
    user = User.new permitted_params

    if user.save
      redirect_to root_path
      flash[:success] = 'user created!'

    else
      flash[:danger] = 'creating error!'
      render :new, locals: { user: user }
    end
  end

  def edit
    user = User.find(params[:id])

    locals user: user
  end

  def update
    user = User.find(params[:id])

    if user.update permitted_params
      redirect_to root_path
      flash[:success] = 'user updated!'
    else
      flash[:danger] = 'updating error!'
      render :edit, locals: { user: user }
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to root_path
    flash[:success] = 'user deleted!'
  end

  private

  def permitted_params
    params.require(:user).permit(:user_name, :user_email, :birthdate, :phone_number)
  end
end
