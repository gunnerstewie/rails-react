# frozen_string_literal: true

class User < ApplicationRecord
  validates :user_name, presence: true
  validates :user_email, format: { with: URI::MailTo::EMAIL_REGEXP }, presence: true
end
