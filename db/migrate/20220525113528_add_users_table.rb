# frozen_string_literal: true

class AddUsersTable < ActiveRecord::Migration[6.1]
  def change
    create_table 'users', id: :uuid, default: -> { 'gen_random_uuid()' }, force: :cascade do |t|
      t.string 'user_name'
      t.string 'user_email'
      t.date 'birthdate'
      t.integer 'phone_number'

      t.timestamps null: false
    end
  end
end
