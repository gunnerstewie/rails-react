# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:user_email) { |i| "user-#{i}@example.com" }
    user_name { 'Alex' }
  end
end
