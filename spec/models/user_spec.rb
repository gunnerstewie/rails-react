# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to allow_value('index@gmail.com').for(:user_email) }
  it { is_expected.not_to allow_value('index').for(:user_email) }
  it { is_expected.to validate_presence_of(:user_name) }
  it { is_expected.to validate_presence_of(:user_email) }
end
