# frozen_string_literal: true

RSpec.describe 'GET api/users', type: :request do
  let(:path) { '/api/users' }
  let(:user) { create(:user) }

  context 'when requested index page' do
    it 'returns list of users' do
      get path

      expect(response).to have_http_status :success
      expect(response.content_type).to eq('application/json; charset=utf-8')
    end
  end
end
