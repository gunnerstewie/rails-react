# frozen_string_literal: true

RSpec.describe 'DELETE api/user/:id', type: :request do
  let(:path) { "/api/users/#{user.id}" }
  let!(:user) { create(:user) }

  context 'when selected user on table' do
    it 'check that database not empty and user is present' do
      expect(User.count).to eq(1)
      expect(User.last.user_name).to eq('Alex')
    end
    it 'destroyed' do
      delete path

      expect(response).to have_http_status :no_content
      expect(User.count).to eq(0)
      expect(body).to eq('')
    end
  end
end
