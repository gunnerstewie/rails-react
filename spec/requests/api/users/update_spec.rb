# frozen_string_literal: true

RSpec.describe 'PUT api/users/:id', type: :request do
  let(:path) { "/api/users/#{user.id}" }
  let!(:user) { create(:user) }
  let(:params) { { user: user_params } }

  let(:user_params) do
    {
      user_name: 'Ivan',
      user_email: 'example11@gmail.com'
    }
  end

  context 'when updated user with valid params' do
    it 'check that database not empty and user is present' do
      expect(User.count).to eq(1)
      expect(User.last.user_name).to eq('Alex')
    end

    it 'update user with new params' do
      put path, params: params
      expect(response).to have_http_status :success
      user.reload
      expect(user.user_name).to eq('Ivan')
      expect(user.user_email).to eq('example11@gmail.com')
    end
  end

  context 'when invalid params given' do
    let(:user_params) do
      {
        user_name: '',
        user_email: 'example11@gmail.com'
      }
    end

    it 'do not update user with new params' do
      put path, params: params
      expect(response).to have_http_status :success
      user.reload
      expect(user.user_name).not_to eq('Ivan')
      expect(user.user_email).not_to eq('example11@gmail.com')
      expect(user.user_name).to eq('Alex')
    end
  end
end
