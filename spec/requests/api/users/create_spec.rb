# frozen_string_literal: true

RSpec.describe 'POST api/users', type: :request do
  let(:path) { '/api/users' }

  let(:params) { { user: user_params } }

  let(:user_params) do
    {
      user_name: 'Andrey',
      user_email: 'example11@gmail.com'
    }
  end

  context 'when valid params given' do
    it 'check that database is empty' do
      expect(User.count).to eq(0)
    end

    it 'create user' do
      post path, params: params

      expect(response).to have_http_status :success
      expect(response.content_type).to eq('application/json; charset=utf-8')
      expect(User.count).to eq(1)
    end

    it 'response have given params' do
      post path, params: params

      expect(response.body).to include('Andrey')
      expect(response.body).to include('example11@gmail.com')
    end
  end

  context 'when invalid params given' do
    let(:user_params) do
      {
        user_name: '',
        user_email: 'example11@gmail.com'
      }
    end

    it 'do not create user' do
      post path, params: params

      expect(response).to have_http_status :ok
      expect(User.count).to eq(0)
    end
  end
end
